package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Pizza;

import java.util.List;

public class CommandeDto {

    private long id;
    private String name;
    private List<Pizza> pizzas;

    public CommandeDto() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

}
