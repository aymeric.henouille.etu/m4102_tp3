package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class PizzaCreateDto {

    private String name;
    private List<Ingredient> ingredients;

    public PizzaCreateDto() { this.ingredients = new ArrayList<>(); }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getIngredients() { return this.ingredients; }

}
