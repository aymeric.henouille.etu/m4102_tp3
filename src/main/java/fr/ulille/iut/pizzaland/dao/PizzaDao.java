package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropPizzaTable();

    @SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
    @GetGeneratedKeys
    long insert(String name);

    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @Transaction
    default Pizza getPizzaById(long id) {
        Pizza p = findById(id);
        IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
        List<Ingredient> li = new ArrayList<>();
        for (long idi : getIngredientsFromPizza(p.getId())) {
            li.add(ingredientDao.findById(idi));
        }
        p.setIngredients(li);

        return p;
    }

    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(long id);

    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(String name);

    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void remove(long id);

    // Association
    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza INTEGER, idIngredient INTEGER, PRIMARY KEY (idPizza, idIngredient), FOREIGN KEY (idPizza) REFERENCES pizzas(id), FOREIGN KEY (idIngredient) REFERENCES ingredients(id))")
    void createAssociationTable();

    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();

    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:idPizza, :idIngredient)")
    @GetGeneratedKeys
    long insert(long idPizza, long idIngredient);

    @Transaction
    default List<Pizza> getAllPizzas() {
        List<Pizza> l = getAll();
        IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
        for (Pizza p : l) {
            List<Ingredient> li = new ArrayList<>();
            for (long id : getIngredientsFromPizza(p.getId())) {
                li.add(ingredientDao.findById(id));
            }
            p.setIngredients(li);
        }
        return l;
    }

    @SqlQuery("SELECT idIngredient FROM PizzaIngredientsAssociation where idPizza = :idPizza")
    @RegisterBeanMapper(Ingredient.class)
    List<Long> getIngredientsFromPizza(long idPizza);

    @SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE idPizza = :idPizza")
    void removeFromAssociation(long idPizza);

    @Transaction
    default void createTableAndIngredientAssociation() {
        createPizzaTable();
        createAssociationTable();
    }

    @Transaction
    default void dropTableAndIngredientAssociation() {
        dropPizzaTable();
        dropAssociationTable();
    }

}
