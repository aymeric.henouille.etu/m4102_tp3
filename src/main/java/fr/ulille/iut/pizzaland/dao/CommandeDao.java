package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

public interface CommandeDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createCommandeTable();

    @SqlUpdate("DROP TABLE IF EXISTS commandes")
    void dropCommandeTable();

    @SqlUpdate("INSERT INTO commandes (name) VALUES (:name)")
    @GetGeneratedKeys
    long insert(String name);

    @SqlQuery("SELECT * FROM commandes")
    @RegisterBeanMapper(Commande.class)
    List<Commande> getAll();

    @Transaction
    default Commande getCommandeById(long id) {
        Commande c = findById(id);
        PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
        List<Pizza> l = new ArrayList<>();
        for (long idi : getPizzaFromCommande(c.getId())) {
            l.add(pizzaDao.findById(idi));
        }
        c.setPizzas(l);

        return c;
    }

    @SqlQuery("SELECT * FROM commandes WHERE id = :id")
    @RegisterBeanMapper(Commande.class)
    Commande findById(long id);

    @SqlQuery("SELECT * FROM commandes WHERE name = :name")
    @RegisterBeanMapper(Commande.class)
    Commande findByName(String name);

    @SqlUpdate("DELETE FROM commandes WHERE id = :id")
    void remove(long id);

    // Association
    @SqlUpdate("CREATE TABLE IF NOT EXISTS CommandePizzaAssociation (idCommande INTEGER, idPizza INTEGER, PRIMARY KEY (idCommande, idPizza), FOREIGN KEY (idCommande) REFERENCES commandes(id), FOREIGN KEY (idPizza) REFERENCES pizzas(id))")
    void createAssociationTable();

    @SqlUpdate("DROP TABLE IF EXISTS CommandePizzaAssociation")
    void dropAssociationTable();

    @SqlUpdate("INSERT INTO CommandePizzaAssociation (idCommande, idPizza) VALUES (:idCommande, :idPizza)")
    @GetGeneratedKeys
    long insert(long idCommande, long idPizza);

    @Transaction
    default List<Commande> getAllCommandes() {
        List<Commande> l = getAll();
        PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
        for (Commande c : l) {
            List<Pizza> lp = new ArrayList<>();
            for (long id : getPizzaFromCommande(c.getId())) {
                lp.add(pizzaDao.findById(id));
            }
            c.setPizzas(lp);
        }
        return l;
    }

    @SqlQuery("SELECT idPizza FROM CommandePizzaAssociation where idCommande = :idCommande")
    @RegisterBeanMapper(Pizza.class)
    List<Long> getPizzaFromCommande(long idCommande);

    @SqlUpdate("DELETE FROM CommandePizzaAssociation WHERE idCommande = :idCommande")
    void removeFromAssociation(long idCommande);

    @Transaction
    default void createTableAndPizzaAssociation() {
        createCommandeTable();
        createAssociationTable();
    }

    @Transaction
    default void dropTableAndPizzaAssociation() {
        dropCommandeTable();
        dropAssociationTable();
    }

}
