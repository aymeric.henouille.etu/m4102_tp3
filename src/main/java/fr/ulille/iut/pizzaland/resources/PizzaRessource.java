package fr.ulille.iut.pizzaland.resources;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Path("/pizzas")
public class PizzaRessource {

    final static Logger LOGGER = LoggerFactory.getLogger(PizzaRessource.class);
    private PizzaDao pizzas;

    @Context
    public UriInfo uriInfo;

    public PizzaRessource() {
        pizzas = BDDFactory.buildDao(PizzaDao.class);
        pizzas.createPizzaTable();
    }

    @GET
    public List<PizzaDto> getAll() {
        LOGGER.info("PizzaResource:getAll");

        List<PizzaDto> l = pizzas.getAllPizzas().stream().map(Pizza::toDto).collect(Collectors.toList());
        return l;
    }

    @GET
    @Path("{id}")
    public PizzaDto getOnePizza(@PathParam("id") long id) {
        LOGGER.info("getOnePizza(" + id + ")");
        try {
            Pizza pizza = pizzas.getPizzaById(id);
            return Pizza.toDto(pizza);
        }
        catch ( Exception e ) {
            // Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @POST
    public Response createPizza(PizzaCreateDto pizzaCreateDto) {
        Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
            long id = pizzas.insert(pizza.getName());
            pizza.setId(id);
            PizzaDto pizzaDto = Pizza.toDto(pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pizzaDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }

    @DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") long id) {
        if ( pizzas.findById(id) == null ) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        pizzas.remove(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }

    @GET
    @Path("{id}/name")
    public String getPizzaName(@PathParam("id") long id) {
        Pizza pizza = pizzas.findById(id);
        if ( pizza == null ) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return pizza.getName();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response createPizza(@FormParam("name") String name) {
        Pizza existing = pizzas.findByName(name);
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza pizza = new Pizza();
            pizza.setName(name);

            long id = pizzas.insert(pizza.getName());
            pizza.setId(id);
            PizzaDto pizzaDto = Pizza.toDto(pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pizzaDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }

}
